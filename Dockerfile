FROM openjdk:8-jdk
MAINTAINER Kaustav Kaushik Jaiswal <kaustavj@gmail.com>

ENV GRADLE_VERSION=4.1
ENV ANDROID_BUILD_TOOLS_VERSION=27.0.2
#Not needed anymore as Google messed with the bundle names
ENV ANDROID_SDK_TOOLS_VERSION=27.0.2
ENV ANDROID_VERSION=27

#installing wget and unzip
RUN apt-get update && apt-get -y install wget && apt-get -y install unzip && apt-get -y install zip && apt-get -y install curl

RUN cd /opt/ && wget https://dl.google.com/android/repository/sdk-tools-linux-3859397.zip

RUN cd /opt && wget https://services.gradle.org/distributions/gradle-4.1-bin.zip
RUN cd /opt && unzip /opt/gradle-${GRADLE_VERSION}-bin.zip
RUN cd /opt && ln -s /opt/gradle-${GRADLE_VERSION} gradle

RUN mkdir -p /opt/Android/sdk
RUN cd /opt && unzip sdk-tools-linux-3859397.zip -d /opt/Android/sdk/
ENV GRADLE_HOME=/opt/gradle
ENV ANDROID_HOME=/opt/Android/sdk
ENV PATH="${ANDROID_HOME}/tools:${ANDROID_HOME}/platform-tools:${GRADLE_HOME}/bin:${PATH}"

RUN cd $ANDROID_HOME/tools/bin\
&& ./sdkmanager --licenses\
&& echo 'N'\
&& echo 'y' | ./sdkmanager "build-tools;${ANDROID_BUILD_TOOLS_VERSION}"\
&& echo 'y' | ./sdkmanager "platform-tools" "platforms;android-${ANDROID_VERSION}"\
&& echo 'y' | ./sdkmanager "extras;android;m2repository"\
&& echo 'y' | ./sdkmanager "extras;google;m2repository"\
&& echo 'y' | ./sdkmanager "extras;m2repository;com;android;support;constraint;constraint-layout;1.0.2" --verbose\
&& echo 'y' | ./sdkmanager "extras;m2repository;com;android;support;constraint;constraint-layout-solver;1.0.2" --verbose

RUN apt-get -y install lib32stdc++6
RUN apt-get -y install lib32z1